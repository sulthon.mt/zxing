package com.chewie.libraryzxing;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public class MainActivity extends AppCompatActivity {

    EditText text;
    Button btn;
    ImageView img;
    public final static int QRcodeWidth = 500;
    Bitmap bitmap;
    String EditValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.editText);
        btn = findViewById(R.id.button);
        img = findViewById(R.id.image);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditValue = text.getText().toString();

                try{
                    bitmap = TextToImageEncode(EditValue);

                    img.setImageBitmap(bitmap);
                }catch(WriterException e){
                    e.printStackTrace();
                }
            }
        });
    }

    Bitmap TextToImageEncode(String Values) throws WriterException{
        BitMatrix bm;
        try{
            bm = new MultiFormatWriter().encode(
            Values,
                   BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QRcodeWidth, QRcodeWidth, null
                    );
        }catch (IllegalArgumentException e){
            return null;
        }
        int bitMatrixWidth = bm.getWidth();
        int bitMatrixHeight = bm.getHeight();

        int[] pixels = new int[bitMatrixWidth *  bitMatrixHeight];

        for (int y = 0 ; y< bitMatrixHeight;y++){
            int offset = y*bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth ; x++){
                pixels[offset + x] = bm.get(x, y)?
                        getResources().getColor(R.color.CodeBlackColor):getResources().getColor(R.color.CodeWhiteColor);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

}
